# CLONE OF BARCELONA CODE SCHOOL

# Stack:
HTML/CSS project that clones Barcelona Code School webpage.


# Functionality: 
Includes nav bar with hover dropdown, embedded youtube video and is responive to a smaller screen width.


# Deployment:
See it at:
  https://pandorajk.gitlab.io/bcs_cloning-project_2/


# Set up and Installation: 
-git clone https://gitlab.com/Pandorajk/bcs_cloning-project_2.git
-open index.html page to see in browser. 


# Commit history: 
1. Initial commit - (04/09/2019)
2. bcs homepg with header, nav, main & info
3. mostly complete, lacks various finishing touches
4. almost final
5. dropdown added on hover


# Contact:
Pandora Jane Knocker - pjknocker@yahoo.com